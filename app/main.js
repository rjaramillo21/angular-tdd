angular.module('Demo', [])
	.service('contactService', ['$http', function($http) {
		this.contacts = [];
		var contactService = this;

		$http.get('http://localhost:9021/contacts').then(function(res) {
			while (res.data[0]) {
				contactService.contacts.push(res.data.pop());
			}
		});
	}])
	.controller('contactController', ['contactService', '$scope', function(contactService, $scope) {
		$scope.contacts = contactService.contacts;
	}])
	.filter('proper', function() {
		return function(name) {
			var type = typeof name;
			if(type !== 'string' && type !== 'number') throw new Error();
			return name.toString().split(" ").map(function(word) {
				return word[0].toUpperCase().concat(word.slice(1));
			}).join(" ");
		}
	});