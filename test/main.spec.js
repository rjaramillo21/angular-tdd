var assert = chai.assert;
var expect = chai.expect;

describe('The Address Book App', function() {
	
	describe('the contact service', function() {
		beforeEach(function() {
			module('Demo');
			inject(function($injector) {
				contactService = $injector.get("contactService");
				$httpBackend = $injector.get("$httpBackend");
			});	
		});
	
		it('should have a property contact, an array', function() {
			expect(contactService.contacts).to.be.an('array');
		});

		it('should call the backend', function() {
			$httpBackend.expectGET('http://localhost:9021/contacts')
				.respond(200, []);
			$httpBackend.flush();
		});
	});

	describe('the contact controller', function() {
		beforeEach(function() {
			module('Demo');
			inject(function($injector, $rootScope) {
				$scope = $rootScope.$new(); 
				contactService = $injector.get("contactService");
				$httpBackend = $injector.get("$httpBackend");
				$controller = $injector.get("$controller");
			})
		});

		it('should store an array of contacts in scope', function() {
			$controller('contactController', {contactService: contactService, $scope: $scope });
			assert.isArray($scope.contacts);
		});
	});

	describe('the proper filter', function() {
		beforeEach(function() {
			module('Demo');
			inject(function($injector) {
				proper = $injector.get("$filter")("proper");
			});
		});

		it('should proper case a string', function() {
			expect(proper("rodolfo jaramillo")).to.equal("Rodolfo Jaramillo");
		});

		it('should take a number an return it as a string', function() {
			expect(proper(21)).to.equal("21");
		});

		it('should throw and error on incompatible type ', function() {
			assert.throws(function() {
				proper(undefined);
			});
		});
	});
});