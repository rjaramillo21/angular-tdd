var express = require('express');
var cors = require('cors');
var app = express();
app.use(cors());
var contacts = [{
	name: 'Rodolfo'
}, {
	name: 'Made'
}];

app.get('/contacts', function(req, res) {
	res.status(200).json(contacts);
});

var port = 9021;
app.listen(port, function() {
	console.log('Listening Port ' + port);
});